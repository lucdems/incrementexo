<?php

namespace App\Controller;

use App\Entity\NumberIncrement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        // On récupère le seul nombre de la table en base de données et qui possède l'id 1 (créé auparavant)
        $nombre = $em->getRepository(NumberIncrement::class)->find(1);

        return $this->render('increment.html.twig',
            array('number' => $nombre));
    }

    /**
     * @Route("/incrementAPI", name="incrementApi")
     */
    public function increment()
    {
        $reponse = new JsonResponse();
        $em = $this->getDoctrine()->getManager();

        // On récupère le seul nombre de la table en base de données et qui possède l'id 1 (créé auparavant)
        $nombre = $em->getRepository(NumberIncrement::class)->find(1);

        // On incrémente le nombre
        $valeurIncrementee = $nombre->getNumber() + 1;
        $nombre->setNumber($valeurIncrementee);

        try{
            $em->persist($nombre);
            $em->flush();
        }
        catch(\Exception $e){
            $reponse->setData(array('state' => 'error'))
                    ->setStatusCode(Response::HTTP_BAD_REQUEST);
            return $reponse;
        }

        $reponse->setData(array('newValue' => $nombre->getNumber()));
        return $reponse;

    }
}
