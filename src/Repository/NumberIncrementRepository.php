<?php

namespace App\Repository;

use App\Entity\NumberIncrement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NumberIncrement|null find($id, $lockMode = null, $lockVersion = null)
 * @method NumberIncrement|null findOneBy(array $criteria, array $orderBy = null)
 * @method NumberIncrement[]    findAll()
 * @method NumberIncrement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NumberIncrementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NumberIncrement::class);
    }

    // /**
    //  * @return NumberIncrement[] Returns an array of NumberIncrement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NumberIncrement
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
